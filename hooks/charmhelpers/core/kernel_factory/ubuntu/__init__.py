
from .. import KernelBase
from subprocess import check_call


class Kernel(KernelBase):
    '''
    Implementation of KernelBase for Ubuntu
    '''

    def _modprobe(self, module, persist=True):
        if persist:
            with open('/etc/modules', 'r+') as modules:
                if module not in modules.read():
                    modules.write(module)

    def _update_initramfs(self, version='all'):
        """Updates an initramfs image"""
        return check_call(["update-initramfs", "-k", version, "-u"])
