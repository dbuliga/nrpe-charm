import os
import re

from .. import KernelBase
from subprocess import check_call, check_output
from charmhelpers.core.hookenv import (
    log,
    INFO
)


class Kernel(KernelBase):
    '''
    Implementation of KernelBase for CentOS
    '''

    def _modprobe(self, module, persist=True):
        if persist:
            if not os.path.exists('/etc/rc.modules'):
                open('/etc/rc.modules', 'a').close()
                os.chmod('/etc/rc.modules', 0111)
            with open('/etc/rc.modules', 'r+') as modules:
                if module not in modules.read():
                    modules.write('modprobe %s\n' % module)

    def _update_initramfs(self, version='all'):
        """Updates an initramfs image"""
        return check_call(["dracut", "-f", version])
