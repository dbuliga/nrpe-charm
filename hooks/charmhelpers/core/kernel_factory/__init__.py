import importlib
import re

from charmhelpers import get_platform
from subprocess import check_call, check_output
from charmhelpers.core.hookenv import (
    log,
    INFO
)


class KernelBase(object):

    def modprobe(self, module, persist=True):
        cmd = ['modprobe', module]
        log('Loading kernel module %s' % module, level=INFO)
        check_call(cmd)
        return self._modprobe(module, persist)

    def _modprobe(self, module, persist=True):
        raise NotImplementedError()

    def rmmod(self, module, force=False):
        """Remove a module from the linux kernel"""
        cmd = ['rmmod']
        if force:
            cmd.append('-f')
        cmd.append(module)
        log('Removing kernel module %s' % module, level=INFO)
        return check_call(cmd)

    def lsmod(self):
        return check_output(['lsmod'],
                            universal_newlines=True)

    def is_module_loaded(self, module):
        """Checks if a kernel module is already loaded"""
        matches = re.findall('^%s[ ]+' % module, self.lsmod(), re.M)
        return len(matches) > 0

    def update_initramfs(self, version='all'):
        return self._update_initramfs(version)

    def _update_initramfs(self, version='all'):
        raise NotImplementedError()

module = "charmhelpers.core.kernel_factory.%s" % get_platform()
kernel = importlib.import_module(module)


class Kernel(kernel.Kernel):
    pass
