#!/usr/bin/env python
# -*- coding: utf-8 -*-

# Copyright 2014-2015 Canonical Limited.
#
# This file is part of charm-helpers.
#
# charm-helpers is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License version 3 as
# published by the Free Software Foundation.
#
# charm-helpers is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with charm-helpers.  If not, see <http://www.gnu.org/licenses/>.


from charmhelpers.core.kernel_factory import Kernel

__author__ = "Jorge Niedbalski <jorge.niedbalski@canonical.com>"

kernel = Kernel()


def modprobe(module, persist=True):
    """Load a kernel module and configure for auto-load on reboot."""
    kernel.modprobe(module, persist)


def rmmod(module, force=False):
    """Remove a module from the linux kernel"""
    return kernel.rmmod(module, force)


def lsmod():
    """Shows what kernel modules are currently loaded"""
    return kernel.lsmod()


def is_module_loaded(module):
    """Checks if a kernel module is already loaded"""
    return kernel.is_module_loaded(module)


def update_initramfs(version='all'):
    """Updates an initramfs image"""
    return kernel.update_initramfs(version)
