from charmhelpers.core import host
from charmhelpers.core import hookenv
from charmhelpers.core.services import helpers


def determine_packages():
    """ List of packages this charm needs installed """
    pkgs = [
        'nagios-nrpe-server',
        'nagios-plugins-basic',
        'nagios-plugins-standard'
    ]
    if hookenv.config('export_nagios_definitions'):
        pkgs.append('rsync')
    return pkgs


def restart_nrpe(service_name):
    """ Restart nrpe """
    host.service_restart('nagios-nrpe-server')


def render_nrpe_template():
    return helpers.render_template(
                    source='nrpe.tmpl',
                    target='/etc/nagios/nrpe.cfg'
                    )
