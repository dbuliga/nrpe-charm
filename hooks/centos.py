from charmhelpers.core import host
from charmhelpers.core import hookenv
from charmhelpers.core.services import helpers


def determine_packages():
    """ List of packages this charm needs installed """
    pkgs = [
        'epel-release',
        'nagios',
        'nagios-plugins-nrpe',
        'nagios-plugins-all',
        'nrpe'
    ]
    if hookenv.config('export_nagios_definitions'):
        pkgs.append('rsync')
    return pkgs


def restart_nrpe(service_name):
    """ Restart nrpe """
    host.service_restart('nrpe')


def render_nrpe_template():
    return helpers.render_template(
                    source='nrpe-centos.tmpl',
                    target='/etc/nagios/nrpe.cfg'
                    )
